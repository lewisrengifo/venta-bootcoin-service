package com.bootcamp.infraestructure.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** purchase request document. */
@Getter
@Setter
@AllArgsConstructor
@Document(collection = "purchaseRequest-sell")
public class PurchaseRequest {
  @Id private String id;
  private String requesterDocument;
  private String sellerDocument;
  private String totalAmount;
  private String paymentMethod;
  private String accepted;
  private String validated;
}
