package com.bootcamp.events;

import com.bootcamp.infraestructure.document.BankAccounts;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Created event for bank account. */
@Data
@EqualsAndHashCode
public class BankAccountCreatedEvent extends Event<BankAccounts> {}
