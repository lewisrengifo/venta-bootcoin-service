package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.BankAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Mono;

/** Implementation of CustomRepository. */
public class BankAccountCustomRepositoryImpl implements BankAccountCustomRepository {

  private final ReactiveMongoTemplate mongoTemplate;

  @Autowired
  public BankAccountCustomRepositoryImpl(ReactiveMongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public Mono<Boolean> hasSavingsAccount(String document, String saving) {
    Query query =
        new Query(Criteria.where("clientDocument").is(document))
            .addCriteria(Criteria.where("type").is(saving));
    return mongoTemplate.exists(query, BankAccounts.class);
  }

  @Override
  public Mono<Boolean> hasCheckingAccount(String document, String checking) {
    Query query =
        new Query(Criteria.where("clientDocument").is(document))
            .addCriteria(Criteria.where("type").is(checking));
    return mongoTemplate.exists(query, BankAccounts.class);
  }

  @Override
  public Mono<Boolean> hasBankAccount(String document) {

    Query query = new Query(Criteria.where("clientDocument").is(document));
    return mongoTemplate.exists(query, BankAccounts.class);
  }
}
