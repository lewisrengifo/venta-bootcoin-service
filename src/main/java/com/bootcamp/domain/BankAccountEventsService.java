package com.bootcamp.domain;

import com.bootcamp.events.BankAccountCreatedEvent;
import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.events.PurchaseRequestCreatedEvent;
import com.bootcamp.infraestructure.document.BankAccounts;
import com.bootcamp.infraestructure.repository.BankAccountRepository;
import java.util.Date;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/** events service bank account. */
@Component
@Slf4j
public class BankAccountEventsService {

  private BankAccountRepository bankAccountRepository;

  public BankAccountEventsService(BankAccountRepository bankAccountRepository) {
    this.bankAccountRepository = bankAccountRepository;
  }

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.bankAccount.name:bankAccounts}")
  private String topicBankAccount;

  public void publish(BankAccounts bankAccounts) {
    BankAccountCreatedEvent createdEvent = new BankAccountCreatedEvent();
    createdEvent.setData(bankAccounts);
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setDate(new Date());

    this.producer.send(topicBankAccount, createdEvent);
  }

  @KafkaListener(
      topics = "${topic.bankAccount.name:bankAccounts}",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "11")
  public void consumer(Event<?> event) {
    if (event.getClass().isAssignableFrom(BankAccountCreatedEvent.class)) {
      BankAccountCreatedEvent createdEvent = (BankAccountCreatedEvent) event;
      log.info(
          "Received purchaseRequest created event .... with Id={}, data={}",
          event.getId(),
          event.getData());
      bankAccountRepository.save(createdEvent.getData()).subscribe();
    }
  }
}
