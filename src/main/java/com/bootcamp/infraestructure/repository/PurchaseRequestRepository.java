package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.PurchaseRequest;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/** reactive repository for purchase request. */
@Repository
public interface PurchaseRequestRepository
    extends ReactiveMongoRepository<PurchaseRequest, String> {}
