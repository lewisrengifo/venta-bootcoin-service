package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.BankAccounts;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Reactive repository for bank account.
 */
@Repository
public interface BankAccountRepository
    extends ReactiveMongoRepository<BankAccounts, String>, BankAccountCustomRepository {}
