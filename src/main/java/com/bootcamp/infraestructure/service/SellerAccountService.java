package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.SellerAccount;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** seller account service. */
public interface SellerAccountService {
  Flux<SellerAccount> getAll();

  Mono<SellerAccount> save(SellerAccount purchaseRequest);

  Mono deleteById(String id);

  Mono findById(String id);
}
