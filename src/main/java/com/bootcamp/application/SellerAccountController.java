package com.bootcamp.application;

import com.bootcamp.infraestructure.document.SellerAccount;
import com.bootcamp.infraestructure.service.SellerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/** seller account controller. */
@RestController
@RequestMapping("/sellerAccount")
public class SellerAccountController {

  @Autowired private SellerAccountService sellerAccountService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<SellerAccount> saveSellerAccount(@RequestBody SellerAccount sellerAccount) {
    return sellerAccountService.save(sellerAccount);
  }
}
