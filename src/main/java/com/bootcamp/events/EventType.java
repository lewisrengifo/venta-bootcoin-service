package com.bootcamp.events;

/** num for event type */
public enum EventType {
  CREATED,
  UPDATED,
  DELETED
}
