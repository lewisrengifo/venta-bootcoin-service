package com.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** spring boot app. */
@SpringBootApplication
public class VentaBootcoinServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(VentaBootcoinServiceApplication.class, args);
  }
}
