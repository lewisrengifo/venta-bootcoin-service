package com.bootcamp.domain;

import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.events.PurchaseRequestCreatedEvent;
import com.bootcamp.infraestructure.document.PurchaseRequest;
import com.bootcamp.infraestructure.repository.PurchaseRequestRepository;
import java.util.Date;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/** events service for purchase request. */
@Component
@Slf4j
public class PurchaseRequestEventsService {
  private PurchaseRequestRepository purchaseRequestRepository;

  public PurchaseRequestEventsService(PurchaseRequestRepository purchaseRequestRepository) {
    this.purchaseRequestRepository = purchaseRequestRepository;
  }

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.purchaseRequestVenta.name:purchaseRequestVenta}")
  private String topicBankAccount;

  public void publish(PurchaseRequest purchaseRequest) {
    PurchaseRequestCreatedEvent createdEvent = new PurchaseRequestCreatedEvent();
    createdEvent.setData(purchaseRequest);
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setDate(new Date());

    this.producer.send(topicBankAccount, createdEvent);
  }

  @KafkaListener(
      topics = "${topic.purchaseRequestCompra.name:purchaseRequestCompra}",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "11")
  public void consumer(Event<?> event) {
    if (event.getClass().isAssignableFrom(PurchaseRequestCreatedEvent.class)) {
      PurchaseRequestCreatedEvent createdEvent = (PurchaseRequestCreatedEvent) event;
      log.info(
          "Received purchaseRequest created event .... with Id={}, data={}",
          event.getId(),
          event.getData());
      purchaseRequestRepository.save(createdEvent.getData()).subscribe();
    }
  }
}
