package com.bootcamp.events;

import com.bootcamp.infraestructure.document.PurchaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class PurchaseRequestCreatedEvent extends Event<PurchaseRequest> {}
