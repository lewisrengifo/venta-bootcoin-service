package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.SellerAccount;
import com.bootcamp.infraestructure.repository.BankAccountRepository;
import com.bootcamp.infraestructure.repository.SellerAccountRepository;
import com.bootcamp.infraestructure.service.SellerAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** seller account service implementation. */
@Slf4j
@Service
public class SellerAccountServiceImpl implements SellerAccountService {

  @Autowired private SellerAccountRepository sellerAccountRepository;

  @Autowired private BankAccountRepository bankAccountRepository;

  @Override
  public Flux<SellerAccount> getAll() {
    return sellerAccountRepository.findAll();
  }

  @Override
  public Mono<SellerAccount> save(SellerAccount sellerAccount) {
    return bankAccountRepository
        .hasBankAccount(sellerAccount.getClientDocument())
        .flatMap(
            result -> {
              if (result) {
                return sellerAccountRepository.save(sellerAccount);
              } else {
                return Mono.empty();
              }
            });
  }

  @Override
  public Mono deleteById(String id) {
    return sellerAccountRepository.deleteById(id);
  }

  @Override
  public Mono findById(String id) {
    return sellerAccountRepository.findById(id);
  }
}
