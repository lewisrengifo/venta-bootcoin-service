package com.bootcamp.events;

import java.util.Date;
import lombok.Data;

/**
 * abstrac class for event.
 *
 * @param <T>
 */
@Data
public abstract class Event<T> {
  private String id;
  private Date date;
  private EventType type;
  private T data;
}
