package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.PurchaseRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** purchase request service. */
public interface PurchaseRequestService {

  Flux<PurchaseRequest> getAll();

  Mono<PurchaseRequest> save(PurchaseRequest purchaseRequest);

  Mono deleteById(String id);

  Mono findById(String id);
}
