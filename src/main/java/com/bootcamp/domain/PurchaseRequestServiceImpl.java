package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.PurchaseRequest;
import com.bootcamp.infraestructure.repository.PurchaseRequestRepository;
import com.bootcamp.infraestructure.service.PurchaseRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** implementation purchase request service. */
@Slf4j
@Service
public class PurchaseRequestServiceImpl implements PurchaseRequestService {

  @Autowired private PurchaseRequestRepository purchaseRequestRepository;

  @Override
  public Flux<PurchaseRequest> getAll() {
    return purchaseRequestRepository.findAll();
  }

  @Override
  public Mono<PurchaseRequest> save(PurchaseRequest purchaseRequest) {
    return purchaseRequestRepository.save(purchaseRequest);
  }

  @Override
  public Mono deleteById(String id) {
    return purchaseRequestRepository.deleteById(id);
  }

  @Override
  public Mono findById(String id) {
    return purchaseRequestRepository.findById(id);
  }
}
