package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.SellerAccount;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/** seller account repository. */
@Repository
public interface SellerAccountRepository extends ReactiveMongoRepository<SellerAccount, String> {}
