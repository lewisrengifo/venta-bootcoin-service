package com.bootcamp.infraestructure.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/** seller account document. */
@Getter
@Setter
@AllArgsConstructor
@Document(collection = "sellerAccount")
public class SellerAccount {
  private String id;
  private String clientName;
  private String clientDocument;
  private String phoneNumber;
  private String imei;
  private String email;
  private BankAccounts bankAccounts;
}
