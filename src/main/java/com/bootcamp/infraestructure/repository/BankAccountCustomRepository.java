package com.bootcamp.infraestructure.repository;

import reactor.core.publisher.Mono;

/**
 * BankAccountCustomRepository for custom implementations.
 */
public interface BankAccountCustomRepository {
  Mono<Boolean> hasSavingsAccount(String document, String saving);

  Mono<Boolean> hasCheckingAccount(String document, String checking);

  Mono<Boolean> hasBankAccount(String document);
}
