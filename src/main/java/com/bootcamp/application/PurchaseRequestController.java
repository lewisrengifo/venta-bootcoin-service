package com.bootcamp.application;

import com.bootcamp.domain.PurchaseRequestEventsService;
import com.bootcamp.infraestructure.document.PurchaseRequest;
import com.bootcamp.infraestructure.service.PurchaseRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/** purchase request controller */
@RestController
@RequestMapping("/purchaseRequest")
public class PurchaseRequestController {

  @Autowired private PurchaseRequestService purchaseRequestService;

  @Autowired private PurchaseRequestEventsService purchaseRequestEventsService;

  @PostMapping("/saveResponsePurchase")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<PurchaseRequest> savePurchaseRequest(@RequestBody PurchaseRequest purchaseRequest) {
    purchaseRequestEventsService.publish(purchaseRequest);
    return purchaseRequestService.save(purchaseRequest);
  }
}
